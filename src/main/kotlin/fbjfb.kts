fun main() {
    val x = Fraction(11.0, 7.0)
    println(x)
    val y = Fraction(6.0, 13.0)
    println(y)
    println(x.add(y))
    println(x.division(y))
    println(x.minus(y))
    println(x.multiply(y))
    println(x.simplify())
    println(y.simplify())

}

class Fraction(val numerator: Double, val denominator: Double) {
    override fun toString(): String {
        return "$numerator $denominator"

    }

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            return (numerator * other.denominator == other.numerator * denominator)
        }
        return false
    }

    fun add(other: Fraction): Fraction {
        val newDenominator = denominator * other.denominator
        val newNumerator1 = newDenominator / denominator * numerator
        val newNumerator2 = newDenominator / other.denominator *  Fraction(newNumerator, newDenominator)
    }

    fun minus(other: Fraction): Fraction {
        return add(Fraction(-1 * other.numerator, other.denominator))
    }
    fun simplify(other: Fraction): Fraction{

    }
    fun divide(other:Fraction): Fraction{
        return multiply(Fraction(other.denominator, other.numerator))
    }